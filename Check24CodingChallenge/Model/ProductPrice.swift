//
//  ProductPrice.swift
//  Check24CodingChallenge
//
//  Created by Albert Adisaputra on 19/03/21.
//

import Foundation
struct ProductPrice: Codable {
    let value: Double?
    let currency: String?
}

// MARK: - GetProductListResponse
struct GetProductListResponse: Codable {
    let header: ListHeader?
    let filters: [String]?
    let products: [Product]?
}


struct ListHeader: Codable {
    let headerTitle, headerDescription: String?
}

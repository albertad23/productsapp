//
//  Product.swift
//  Check24CodingChallenge
//
//  Created by Albert Adisaputra on 19/03/21.
//

import Foundation

struct Product: Codable {
    let names: String?
    let type: String?
    let id: Int?
    let color: String?
    let imageURL: String?
    let colorCode: String?
    let available: Bool
    let freleaseDate: Int?
    let productDescription: String?
    let longDescription: String?
    let rating: Double?
    let price: ProductPrice?
    let product_id: Int?

    enum CodingKeys: String, CodingKey {
        case names, type, id, color, imageURL, colorCode, available, releaseDate
        case productDescription
        case longDescription, rating, price, product_id
    }
}

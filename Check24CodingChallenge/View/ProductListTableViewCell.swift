//
//  ProductListTableViewCell.swift
//  Check24CodingChallenge
//
//  Created by Albert Adisaputra on 19/03/21.
//

import UIKit

final class ProductTableViewCell:  UITableViewCell {

    @IBOutlet weak var productDateLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productDescriptionLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productTitleLabel: UILabel!


    func setCellData(with product: Product) {

        let date = Date(timeIntervalSince1970: Double(product.releaseDate ?? 0))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let stringDate = dateFormatter.string(from: date)

        productDateLabel.text = stringDate
        productTitleLabel.text = product.names
        productPriceLabel.text = String(product.price?.value ?? 0.0) + " " + (product.price?.currency ?? "")
        productDescriptionLabel.text = product.productDescription ?? ""
        productImageView.imageFromURL(urlString: product.imageURL ?? "")
    }
}



extension UIImageView {
    public func imageFromURL(urlString: String) {
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
            if error != nil {
                print(error ?? "No Error")
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.image = image
            })

        }).resume()
    }
}

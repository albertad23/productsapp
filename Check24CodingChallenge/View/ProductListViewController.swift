//
//  ViewController.swift
//  Check24CodingChallenge
//
//  Created by Albert Adisaputra on 19/03/21.
//

import UIKit

final class ProductListViewController: UIViewController {

    let productCellIdentifier = "ProductTableViewCell"
    let viewModel = ProductViewModel()

    @IBOutlet var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        viewModel.getProduct {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(pullToRefresh), for: UIControl.Event.valueChanged)
        return refreshControl
    }()

    @objc private func pullToRefresh() {
        viewModel.getProduct {
            DispatchQueue.main.async {
                self.tableView.refreshControl?.endRefreshing()
                self.tableView.reloadData()
            }
        }
    }

    func setupTableView() {
        tableView.dataSource = self
        tableView.refreshControl = refreshControl
        tableView.tableFooterView = UIView()
        self.tableView.register(UINib(nibName: productCellIdentifier, bundle: nil), forCellReuseIdentifier: productCellIdentifier)
    }

    @IBAction func didChangeSegment(_ sender: UISegmentedControl) {
        viewModel.segmentedControlDidChange(with: sender.selectedSegmentIndex) {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
}


extension ProductListViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.filteredProductList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: productCellIdentifier, for: indexPath) as? ProductTableViewCell else {
            return UITableViewCell()
        }
        cell.setCellData(with: viewModel.filteredProductList[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ProductDetailsViewController")
        navigationController?.pushViewController(vc, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


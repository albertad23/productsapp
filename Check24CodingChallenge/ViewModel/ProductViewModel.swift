//
//  ProductViewModel.swift
//  Check24CodingChallenge
//
//  Created by Albert Adisaputra on 19/03/21.
//

import Foundation

class ProductViewModel {
    var filteredProductList: [Product] = []
    var productList: [Product] = []
    var segmentTextContent: [String] = []
    var heightForRow: Float = 161

    func getProduct(completion: @escaping () -> ()) {
        let networkManager = NetworkManager()
        networkManager.getProduct { response in
            self.productList = response?.products ?? []
            self.filteredProductList = response?.products ?? []
            self.segmentTextContent = response?.filters ?? []
            completion()
        }
    }

    func segmentedControlDidChange(with value: Int, complation: @escaping() -> ()) {
        if value == 1 {
            filteredProductList = productList.filter({$0.available})
        }
        if value == 0 {
            filteredProductList = productList
        }
        complation()
    }
}

//
//  NetworkManager.swift
//  Check24CodingChallenge
//
//  Created by Albert Adisaputra on 19/03/21.
//

import Foundation

final class NetworkManager {

    func getProduct(completion: @escaping (_ data: GetProductListResponse?)->()) {
        // Set up the URL request

        let session = URLSession.shared
        guard let url = URL(string: "https://app.check24.de/products-test.json") else {
            return
        }
        let task = session.dataTask(with: url) { data, response, error in

            guard let data = data, error == nil else {
                print("Client error!")
                return
            }
            do {
                // Decode data to object
                let jsonDecoder = JSONDecoder()
                let product: GetProductListResponse = try jsonDecoder.decode(GetProductListResponse.self, from: data)
                completion(product)

            }

            // Handling all possible error for decoding
            catch let error {
                print(error)
            }
        }
        task.resume()
    }


    func login() {

        //declare parameter as a dictionary which contains string as key and value combination. considering inputs are valid

        let parameters = ["id": 13, "name": "jack"] as [String : Any]

        //create the url with URL
        let url = URL(string: "www.thisismylink.com/postName.php")! //change the url

        //create the session object
        let session = URLSession.shared

        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print(error.localizedDescription)
        }

        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")

        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {
                return
            }

            guard let data = data else {
                return
            }

            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    // handle json...
                }
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()

    }
}
